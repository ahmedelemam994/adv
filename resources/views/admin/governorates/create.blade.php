@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>

  <!-- END: Page CSS-->
@endsection


@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('governorates.index') }}">@lang('governorates')</a>
</li>
<li class="breadcrumb-item active" ><a href="{{ route('governorates.create') }}" >@lang('Add New')</a>
</li>
@endsection


@section('content')
<section id="basic-tabs-components">
    <div class="row match-height">
        <!-- Basic Tabs starts -->
        <div class="col-lg-12">
            <form method="POST" action="{{ route('governorates.store') }}">
            @csrf
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('Add New') @lang('governorate') </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Name')</label>
                                <input type="text" id="fp-default" name="name"  
                                class="form-control "
                                placeholder="@lang('Name')" />
                                @error('name')
                                <div class="text-danger">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Latitude')</label>
                                <input type="text" id="fp-default" name="latitude"  
                                class="form-control "
                                placeholder="@lang('Latitude')" />
                                @error('latitude')
                                <div class="text-danger">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Longitude')</label>
                                <input type="text" id="fp-default" name="longitude"  
                                class="form-control "
                                placeholder="@lang('Longitude')" />
                                @error('longitude')
                                <div class="text-danger">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" class="d-block">@lang('Status')</label>
                                <div class="form-check my-50">
                                    <input type="radio" id="validationRadio3" name="status" checked class="form-check-input" required  value="enabled"/>
                                    <label class="form-check-label" for="validationRadio3">@lang('Enabled')</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" id="validationRadio4" name="status" class="form-check-input" required  value="disabled"/>
                                    <label class="form-check-label" for="validationRadio4">@lang('Disabled')</label>
                                </div>
                            </div>
                        </div>   
                    </div>
                   
                
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary waves-effect waves-float waves-light" type="submit">@lang('Submit')</button>
                </div>
            </div>
           </form>
        </div>
        <!-- Basic Tabs ends -->

    </div>
</section>
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- END: Theme JS-->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endsection