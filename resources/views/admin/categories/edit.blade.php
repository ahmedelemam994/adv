@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>

  <!-- END: Page CSS-->
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('categories.index') }}">@lang('Categories')</a>
</li>
<li class="breadcrumb-item active" ><a href="{{ route('categories.create') }}" >@lang('Modify')</a>
</li>
@endsection

@section('content')
<section id="basic-tabs-components">
    <div class="row match-height">
        <!-- Basic Tabs starts -->
        <div class="col-lg-12">
            <form method="POST" action="{{ route('categories.update', $Category->id) }}">
            @csrf
            @method('PUT')
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('Add New') @lang('Article') </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Name')</label>
                                <input type="text" id="fp-default" name="name" required 
                                class="form-control " value="{{ $Category->name  }}"
                                placeholder="Name" />
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Categories')</label>
                                <select class="select-search form-control" required data-style="btn-default btn-lg" data-width="100%"
                                name="parent_id" data-placeholder="">
                                <option value="0" @if (0 == $Category->parent_id)
                                    selected @endif>لا يوجد تصنيف رئيسي</option>
                                @foreach ($categories as $category)
                                <option value="{{  $category->id }}"  @if ($category->id == $Category->parent_id)
                                    selected @endif>{{  $category->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Filter Groups')</label>
                                <select class="form-control select2" name="Filters[]" multiple="">
                                    @foreach($filterGroups as $group)
                                        <option  @if (in_array($group->id, $item_filtergroups))
                                            selected @endif value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>   
                    </div>
                   
                
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary waves-effect waves-float waves-light" type="submit">@lang('Submit')</button>
                </div>
            </div>
           </form>
        </div>
        <!-- Basic Tabs ends -->

    </div>
</section>
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- END: Theme JS-->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endsection