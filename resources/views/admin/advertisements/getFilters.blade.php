   

@foreach($filterGroups as $filterGroup) 
    <div class="row">
        {{ $filterGroup->name }}
    </div>
    <div class="row">
    @foreach($filterGroup->filters as $filter) 
        <div class="col-md-6 col-12">
            <div class="mb-1">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input ingredient-enable"
                    data-id="{{ $filter->id }}" 
                     id="validationCheckBootstrap"  value="1" />
                    <label class="form-check-label" for="validationCheckBootstrap">
                        {{ $filter->name }}</label>
                    @if ($filterGroup->type == 3)
                    <input type="text" id="fp-default"  data-id="{{ $filter->id }}"
                    name="filters[{{ $filter->id }}]" required 
                    class="form-control ingredient-amount"   disabled/>    
                    @else
                    <input type="hidden" id="fp-default"  data-id="{{ $filter->id }}"
                    name="filters[{{ $filter->id }}]" required 
                    class="form-control ingredient-amount"   disabled/>
                    @endif
                    <div class="invalid-feedback">{{ $filter->name }}</div>
                </div>
            </div>
        </div>
        
    @endforeach     
    </div>
@endforeach


