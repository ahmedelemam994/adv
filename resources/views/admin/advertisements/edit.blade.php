@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/vendors.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/pickers/pickadate/pickadate.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/plugins/forms/pickers/form-pickadate.css">
  <!-- END: Page CSS-->
@endsection


@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('articles.index') }}">@lang('articles')</a>
</li>
<li class="breadcrumb-item active" ><a href="{{ route('articles.edit', $Article->id)}}" >@lang('Modify')</a>
</li>
@endsection
@section('content')

<section id="basic-tabs-components">
    <div class="row match-height">
        <!-- Basic Tabs starts -->
        <div class="col-lg-12">
            <form method="POST" action="{{ route('articles.update', $Article->id) }}"
                 enctype="multipart/form-data">
                 @csrf  

                 @method('PUT')


            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('Modify') @lang('Articles') </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Categories')</label>
                                <select class="select-search form-control" required data-style="btn-default btn-lg" data-width="100%"
                                name="category_id" data-placeholder="">
                                @foreach ($categories as $category)
                                <option value="{{  $category->id }}" @if ($category->id == $Article->category_id)
                                    selected @endif>{{  $category->title }} </option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label for="customFile1" class="form-label">@lang('Image')  </label>
                                <input class="form-control" type="file"  name="image" id="customFile1" required />
                            </div>
                        </div>
                   

                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Date')</label>
                                <input type="text" id="fp-default" name="article_date" required 
                                class="form-control flatpickr-basic"
                                placeholder="YYYY-MM-DD"  value="{{ $Article->article_date }}"/>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach (config('app.languages') as $key => $lang)
                        <li class="nav-item">
                            <a class="nav-link @if ($loop->index == 0) active @endif" 
                                id="{{ $lang }}-tab" data-bs-toggle="tab" href="#{{ $key }}" 
                                aria-controls="{{ $lang }}" role="tab" aria-selected="true">{{ $lang }}</a>
                        </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach (config('app.languages') as $key => $lang)
                        <div class="tab-pane @if ($loop->index == 0) active @endif" id="{{ $key }}" 
                        aria-labelledby="{{ $key }}-tab" role="tabpanel">
                        <div class="row">
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" for="TitleInput">@lang('Title')</label>
                                    <input id="TitleInput" required   name="{{$key}}[title]" class="form-control form-control-lg"
                                     type="text" placeholder="@lang('Title') {{$key}}" value={{ $Article->translate($key)->title ?? '' }}  />
                                     
                                </div>
                                @error($key.'title') 
                                {{  $message }}
                                @enderror
                            </div>
                            <div class="col-12">
                                <div class="mb-1">
                                    <label class="form-label" required for="exampleFormControlTextarea1">وصف</label>
                                    <textarea class="form-control"   name="{{$key}}[content]" id="exampleFormControlTextarea1" rows="3" placeholder="Textarea">{{ $Article->translate($key)->content ?? '' }}</textarea>
                                </div>
                                @error($key.'content') 
                                {{  $content }}
                                @enderror
                            </div>
                        </div>
                            
                        </div>
                        @endforeach
                    </div>
                
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary waves-effect waves-float waves-light" type="submit">@lang('Modify')</button>
                </div>
            </div>
           </form>
        </div>
        <!-- Basic Tabs ends -->

    </div>
</section>
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/forms/pickers/form-pickers.js"></script>
<!-- END: Theme JS-->
@endsection