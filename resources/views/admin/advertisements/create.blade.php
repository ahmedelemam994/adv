@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/vendors.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/pickers/pickadate/pickadate.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/plugins/forms/pickers/form-pickadate.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
   <!-- END: Page CSS-->
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('advertisements.index') }}">@lang('Advertisements')</a>
</li>
<li class="breadcrumb-item active" ><a href="{{ route('advertisements.create') }}" >@lang('Add New')</a>
</li>
@endsection

@section('content')

 <section id="basic-tabs-components">
    <div class="row match-height">
        <!-- Basic Tabs starts -->
        <div class="col-lg-12">
            <form method="POST" action="{{ route('advertisements.store') }}" enctype="multipart/form-data">
            @csrf


            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('Add New') @lang('Advertisements') </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('governorates')</label>
                                <select class="select-search form-control" required data-style="btn-default btn-lg" data-width="100%"
                                name="governorate_id" data-placeholder="" id="governorates">
                                <option value=""></option>
                                @foreach ($governorates as $governorate)
                                <option value="{{  $governorate->id }}">{{  $governorate->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Regions')</label>
                                <select class="select-search form-control" required data-style="btn-default btn-lg" data-width="100%"
                                name="region_id" data-placeholder="" id="regions" >
                                <option value=""></option>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Districts')</label>
                                <select class="select-search form-control" required data-style="btn-default btn-lg" data-width="100%"
                                name="district_id" data-placeholder="" id="districts">
                                <option value=""></option>
                                </select>
                            </div>
                        </div>
                        
                   

                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Name')</label>
                                <input type="text" id="fp-default" name="name" required 
                                class="form-control " placeholder="@lang('Name')" />
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Advertiser')</label>
                                <input type="text" id="fp-default" name="advertiser" required 
                                class="form-control " placeholder="@lang('advertiser')" />
                            </div>
                        </div>

                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Price')</label>
                                <input type="text" id="fp-default" name="price" required 
                                class="form-control " placeholder="@lang('Price')" />
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Categories')</label>
                                <select class="select-search form-control categories" required data-style="btn-default btn-lg" data-width="100%"
                                name="category_id" data-placeholder="" id='categories'>
                                <option value="">اختر</option>
                                @foreach ($categories as $category)
                                <option value="{{  $category->id }}">{{  $category->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-12" >
                            <label class="form-label" for="fp-default" id="filters" >@lang('Filters')</label>
                            {{-- <select class="select-search form-control filters"  
                            required data-style="btn-default btn-lg" data-width="100%" 
                            name="filters[]" data-placeholder="" id="filters" 
                            multiple="multiple">
                            </select> --}}
                        </div>
                    </div>
                
                </div>
               
            </div>
        </div>
            <div class="card-footer">
                <button class="btn btn-primary waves-effect waves-float waves-light" type="submit">@lang('Submit')</button>
            </div>
           </form>
        </div>
        <!-- Basic Tabs ends -->

    </div>
</section> 
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/forms/pickers/form-pickers.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/forms/form-repeater.js"></script>

<!-- END: Theme JS-->

<script>

    $(document).ready(function() {


        $(document).on('change',"#governorates", function () {
            var governorate_id = $(this).val();
            $.ajax({
                type: 'get',
                url: "{{ route('getRegions') }}",
                data: {
                    governorate_id: governorate_id
                },
                success: function(data) {
                    if(data){
                        $('#regions').html(data);
                        $('.select2').select2();
                    }
                }
            });
            
        });

        $(document).on('change',"#regions", function () {
            var region_id = $(this).val();
            $.ajax({
                type: 'get',
                url: "{{ route('getDistricts') }}",
                data: {
                    region_id: region_id
                },
                success: function(data) {
                    if(data){
                        $('#districts').html(data);
                        $('.select2').select2();
                    }
                }
            });
            
        });
        $(document).on('change',"#categories", function () {
            var category_id = $(this).val();
            $.ajax({
                type: 'get',
                url: "{{ route('getFilters') }}",
                data: {
                    category_id: category_id
                },
                success: function(data) {
                    if(data){
                        $('#filters').html(data);

                        $('select.filters').select2({
                        width: '100%',
                        multiple:true,
                       
                        });
                    }
                }
            });
            
        });
        $(document).on('click',".ingredient-enable", function () {
            let id = $(this).attr('data-id')
            let enabled = $(this).is(":checked")
            $('.ingredient-amount[data-id="' + id + '"]').attr('disabled', !enabled)
            $('.ingredient-amount[data-id="' + id + '"]').val(null)
        })
       
    });



</script>

@endsection