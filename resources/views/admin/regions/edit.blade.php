@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
   <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>

  <!-- END: Page CSS-->
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('regions.index') }}">@lang('regions')</a>
</li>
<li class="breadcrumb-item active" ><a href="{{ route('regions.create') }}" >@lang('Modify')</a>
</li>
@endsection

@section('content')
<section id="basic-tabs-components">
    <div class="row match-height">
        <!-- Basic Tabs starts -->
        <div class="col-lg-12">
            <form method="POST" action="{{ route('regions.update', $region) }}">
            @csrf
            @method('PUT')
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('Add New') @lang('Region') </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Name')</label>
                                <input type="text" id="fp-default" name="name"  
                                class="form-control " value="{{ $region->name  }}"
                                placeholder="@lang('Name')" />
                                @error('name')
                                <div class="text-danger">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Latitude')</label>
                                <input type="text" id="fp-default" name="latitude"  
                                class="form-control " value="{{ $region->latitude  }}"
                                placeholder="@lang('Latitude')" />
                                @error('latitude')
                                <div class="text-danger">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('Longitude')</label>
                                <input type="text" id="fp-default" name="longitude"  
                                class="form-control " value="{{ $region->longitude  }}"
                                placeholder="@lang('Longitude')" />
                                @error('longitude')
                                <div class="text-danger">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('governorates')</label>
                                <select class="select-search form-control" required data-style="btn-default btn-lg" data-width="100%"
                                name="governorate_id" data-placeholder="" required>
                                @foreach ($governorates as $governorate)
                                <option value="{{  $governorate->id }}" @if ($governorate->id == $region->governorate_id)
                                    selected @endif>{{  $governorate->name }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>  
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" class="d-block">@lang('Status')</label>
                                <div class="form-check my-50">
                                    <input type="radio" id="validationRadio3" name="status" @if ($region->status == 'enabled')
                                    checked @endif class="form-check-input" required  value="enabled"/>
                                    <label class="form-check-label" for="validationRadio3">@lang('Enabled')</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" @if ($region->status == 'disabled')
                                    checked @endif  id="validationRadio4" name="status" class="form-check-input" required  value="disabled"/>
                                    <label class="form-check-label" for="validationRadio4">@lang('Disabled')</label>
                                </div>
                            </div>
                        </div> 
                    </div>
                   
                
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary waves-effect waves-float waves-light" type="submit">@lang('Submit')</button>
                </div>
            </div>
           </form>
        </div>
        <!-- Basic Tabs ends -->

    </div>
</section>
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- END: Theme JS-->
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
</script>
@endsection