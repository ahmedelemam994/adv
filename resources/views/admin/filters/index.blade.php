@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
  <!-- END: Page CSS-->
@endsection


@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item active"><a href="{{ route('filters.index') }}">@lang('Filters')</a>
</li>
@endsection


@section('content')
<div class="row" id="basic-table">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">@lang('Filters')</h4>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li>
                            <a href="{{route('filters.create')}}"
                               class="btn btn-icon btn-primary waves-effect"
                               data-bs-toggle="tooltip"
                               data-bs-placement="left"
                               title="@lang('Add New')">
                                <i data-feather='plus-square'></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>@lang('Filter')</th>
                        <th class="width-150"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($filterGroups as $filterGroup)
                        <tr>
                            <td>
                                <span class="fw-bold">{{$filterGroup->name}}</span>
                            </td>
                           
                           
                            <td>

                                {{-- <a href="{{route('articles.show',$article)}}" class="btn btn-icon btn-outline-primary waves-effect">
                                    <i data-feather='eye'></i>
                                </a> --}}


                                <a href="{{route('filters.edit',$filterGroup)}}" class="btn btn-icon btn-outline-primary waves-effect">
                                    <i data-feather='edit'></i>
                                </a>
                            

                                {{ html()->form('DELETE',route('filters.destroy',$filterGroup))->class('d-inline')->open()}}
                                <button type="submit" class="btn btn-icon btn-outline-danger waves-effect confirm">
                                    <i data-feather='trash-2'></i>
                                </button>
                                {{ html()->form()->close()}}
                            </td> 
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5" class="text-center"></td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<!-- END: Theme JS-->
@endsection