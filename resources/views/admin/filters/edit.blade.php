@extends('admin.app')

@section('css')
   <!-- BEGIN: Page CSS-->
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/vendors.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/pickers/pickadate/pickadate.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/plugins/forms/pickers/form-flat-pickr.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/plugins/forms/pickers/form-pickadate.css">
   <link rel="stylesheet" type="text/css" href="{{asset('admin')}}/app-assets/css/core/menu/menu-types/vertical-menu.css">
   <!-- END: Page CSS-->
@endsection

@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('home') }}">@lang('Home')</a>
</li>
<li class="breadcrumb-item"><a href="{{ route('filters.index') }}">@lang('filters')</a>
</li>
<li class="breadcrumb-item active" ><a href="{{ route('filters.edit', $filterGroup->id)}}" >@lang('Modify')</a>
</li>
@endsection
@section('content')

<section id="basic-tabs-components">
    <div class="row match-height">
        <!-- Basic Tabs starts -->
        <div class="col-lg-12">
            <form method="POST" action="{{ route('filters.update', $filterGroup) }}"
                 enctype="multipart/form-data">
                 @csrf  

                 @method('PUT')


           <div class="card">
                <div class="card-header">
                    <h4 class="card-title"> @lang('Add New') @lang('Filter') </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        
                   

                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('filter group Name')</label>
                                <input type="text" id="fp-default" name="name" required 
                                class="form-control " placeholder="@lang('Name')"  value="{{  $filterGroup->name }}"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="fp-default">@lang('filter group Type')</label>
                                <select class="select-search form-control" required data-style="btn-default btn-lg" data-width="100%"
                                name="type" data-placeholder="">
                                <option value="1" @if ($filterGroup->type == 1) selected @endif>checkbox</option>
                                <option value="2" @if ($filterGroup->type == 2) selected @endif>tab</option>
                                <option value="3" @if ($filterGroup->type == 3) selected @endif>val</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="mb-1">
                                <label class="form-label" class="d-block">@lang('Status')</label>
                                <div class="form-check my-50">
                                    <input type="radio" id="validationRadio3" name="status" @if ($filterGroup->status == 'enabled')
                                    checked @endif class="form-check-input" required  value="enabled"/>
                                    <label class="form-check-label" for="validationRadio3">@lang('Enabled')</label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" @if ($filterGroup->status == 'disabled')
                                    checked @endif  id="validationRadio4" name="status" class="form-check-input" required  value="disabled"/>
                                    <label class="form-check-label" for="validationRadio4">@lang('Disabled')</label>
                                </div>
                            </div>
                        </div> 
                    </div>
                
                </div>
               
            </div>
            <div class="form-control-repeater">
                <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">filter</h4>
                        </div>
                        <div class="card-body">
                            <div  class="invoice-repeater">
                                <div data-repeater-list="invoice">
                                    @foreach($filters as $filter)
                                    <div data-repeater-item>
                                        <div class="row d-flex align-items-end">
                                            <div class="col-md-6 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" for="itemname"> filter Name</label>
                                                    <input type="text" name="name" class="form-control" 
                                                    id="itemname" aria-describedby="itemname" placeholder="filter Name" value="{{ $filter->name }}" />
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-12">
                                                <div class="mb-1">
                                                    <label class="form-label" class="d-block">@lang('Status')</label>
                                                    <div class="form-check my-50">
                                                        <input type="radio" id="validationRadio3" name="status" @if ($filter->status == 'enabled')
                                                        checked @endif class="form-check-input" required  value="enabled"/>
                                                        <label class="form-check-label" for="validationRadio3">@lang('Enabled')</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input type="radio" id="validationRadio4" name="status" @if ($filter->status == 'disabled')
                                                        checked @endif class="form-check-input" required  value="disabled"/>
                                                        <label class="form-check-label" for="validationRadio4">@lang('Disabled')</label>
                                                    </div>
                                                </div>
                                            </div>   
                                            {{-- <div class="col-md-1 col-12">
                                                <div class="mb-1">
                                                    <div class="form-check">
                                                        <input type="checkbox" class="form-check-input" name="val"
                                                         id="validationCheckBootstrap"  value="1" />
                                                        <label class="form-check-label" for="validationCheckBootstrap">
                                                            Val</label>
                                                        <div class="invalid-feedback">Val</div>
                                                    </div>
                                                </div>
                                            </div> --}}

                                           

                                            <div class="col-md-2 col-12 mb-50">
                                                <div class="mb-1">
                                                    <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                        <i data-feather="x" class="me-25"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <hr />
                                    </div>
                                    @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Add New</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary waves-effect waves-float waves-light" type="submit">@lang('Submit')</button>
            </div>
           </form>
        </div>
        <!-- Basic Tabs ends -->

    </div>
</section>
@endsection

@section('js')
<!-- BEGIN: Theme JS-->
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/components/components-navs.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/forms/pickers/form-pickers.js"></script>
<script src="{{asset('admin')}}/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>
<script src="{{asset('admin')}}/app-assets/js/scripts/forms/form-repeater.js"></script>

<!-- END: Theme JS-->

@endsection