  <div style="clear: both"></div>

<div class="row">
  @foreach ($filterGroups as $filterGroup)
  <div>
    <h1>  {{ $filterGroup->name }} </h1> 
    @foreach ($filterGroup->filters as $filter)
    <a class=""  data-id="{{ $filter->id }}">{{ $filter->name }} /</a> 
    @endforeach 
   
  </div>
  @endforeach 
</div>
<div style="clear: both"></div>

  @foreach ($advertisements as $advertisement)
    <div class="col-md-12">
      <div class="card flex-md-row mb-4 box-shadow h-md-250">
        <div class="card-body d-flex flex-column align-items-start">
          <strong class="d-inline-block mb-2 text-primary">{{ $advertisement->name }}</strong>
          <h3 class="mb-0">
            <a class="text-dark" href="#">{{ $advertisement->advertiser }}</a>
          </h3>
          <div class="mb-1 text-muted">{{ $advertisement->price }}</div>
        
        </div>
        <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="{{ $advertisement->name }}">
      </div>
    </div>
  @endforeach
