<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Blog Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('web')}}/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
    <link href="{{asset('web')}}/assets/blog.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
     

      <div class="nav-scroller py-1 mb-2">
          @foreach ($categories as $category)
          <a class="p-2 text-muted hrefs categories" data-params="category_id"  data-id="{{ $category->id }}">{{ $category->name }}</a>
          @endforeach
          <input type="hidden" id="category_id">
          <input type="hidden" id="governorate_id">
          <input type="hidden" id="region_id">
          <input type="district_id" id="district_id">
      </div>


      <div class="row" id="datas">
        @foreach ($advertisements as $advertisement)
        <div class="col-md-12">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-primary">{{ $advertisement->name }}</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">{{ $advertisement->advertiser }}</a>
              </h3>
              <div class="mb-1 text-muted">{{ $advertisement->price }}</div>
            
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="{{ $advertisement->name }}">
          </div>
        </div>
        @endforeach
      </div>
    </div>

   

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{asset('web')}}/jquery-2.2.4.min.js"></script>
    <script src="{{asset('web')}}/assets/js/vendor/popper.min.js"></script>
    <script src="{{asset('web')}}/js/bootstrap.min.js"></script>
    <script src="{{asset('web')}}/assets/js/vendor/holder.min.js"></script>
    <script>
       $(document).ready(function() {
       
        $(document).on('click',".categories", function () {
            var category_id = $(this).attr('data-id');
            $('#category_id').val(category_id);
            $.ajax({
                type: 'get',
                url: "{{ route('getgovernorates') }}",
                data: {
                  category_id: category_id
                },
                success: function(data) {
                    if(data){
                        $('#datas').html(data);
                    }
                }
            });
          }); 

          $(document).on('click',".governorates", function () {
            var category_id = $('#category_id').val();
            var governorate_id = $(this).attr('data-id');

            $('#governorate_id').val(governorate_id);
            $.ajax({
                type: 'get',
                url: "{{ route('getregions') }}",
                data: {
                  category_id: category_id,
                  governorate_id: governorate_id

                },
                success: function(data) {
                    if(data){
                        $('#datas').html(data);
                    }
                }
            });
          }); 

          $(document).on('click',".regions", function () {
            var category_id = $('#category_id').val();
            var governorate_id = $('#governorate_id').val();
            var region_id = $(this).attr('data-id');
            $('#region_id').val(region_id);
            $.ajax({
                type: 'get',
                url: "{{ route('getdistricts') }}",
                data: {
                  category_id: category_id,
                  governorate_id: governorate_id,
                  region_id:region_id

                },
                success: function(data) {
                    if(data){
                        $('#datas').html(data);
                    }
                }
            });
          }); 



          $(document).on('click',".districts", function () {
            var category_id = $('#category_id').val();
            var governorate_id = $('#governorate_id').val();
            var region_id = $('#region_id').val();
            var district_id = $(this).attr('data-id');
            $('#district_id').val(district_id);
            $.ajax({
                type: 'get',
                url: "{{ route('getdata') }}",
                data: {
                  category_id: category_id,
                  governorate_id: governorate_id,
                  region_id:region_id,
                  district_id:district_id
                },
                success: function(data) {
                    if(data){
                        $('#advertisements').html(data);
                    }
                }
            });
          }); 

        });

    </script>
    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
    <script>
        var CURRENT_URL = location.href;
    </script>
  </body>
</html>

