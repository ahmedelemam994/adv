<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

         Schema::create('filters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->enum('status', ['enabled', 'disabled']);
            $table->integer('filter_group_id')->unsigned();
            $table->foreign('filter_group_id')->references('id')->on('filter_groups')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filters');
    }
};
