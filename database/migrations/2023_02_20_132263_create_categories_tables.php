<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

      



        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('name');
            $table->enum('estate', ['all', 'sale', 'rent']);
            $table->enum('status', ['enabled', 'disabled']);
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('category_filter_group', function (Blueprint $table) {
            $table->id();
            $table->integer('filter_group_id')->unsigned();
            $table->foreign('filter_group_id')->references('id')->on('filter_groups')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });


     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
