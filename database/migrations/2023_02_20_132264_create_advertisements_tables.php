<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

     

        Schema::create('advertisements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('advertiser');
            $table->integer('price');
            $table->enum('status', ['enabled', 'disabled']);
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('governorate_id')->unsigned();
            $table->foreign('governorate_id')->references('id')->on('governorates')->onDelete('cascade');
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');
            $table->integer('district_id')->unsigned();
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });

        // Schema::create('advertisement_filter_group', function (Blueprint $table) {
        //     $table->id();
        //     $table->integer('val')->default(0);
        //     $table->integer('filter_group_id')->unsigned();
        //     $table->foreign('filter_group_id')->references('id')->on('filter_groups')->onDelete('cascade');
        //     $table->integer('advertisement_id')->unsigned();
        //     $table->foreign('advertisement_id')->references('id')->on('advertisements')->onDelete('cascade');
        // });
        

        Schema::create('advertisement_filter', function (Blueprint $table) {
            $table->id();
            $table->integer('filter_id')->unsigned();
            $table->foreign('filter_id')->references('id')->on('filters')->onDelete('cascade');
            $table->integer('advertisement_id')->unsigned();
            $table->foreign('advertisement_id')->references('id')->on('advertisements')->onDelete('cascade');
        });
        

       
  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertisements');
    }
};
