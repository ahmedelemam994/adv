<?php

use App\Http\Controllers\Api\ApiController;

// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\Api\GovernoratesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('api')
->namespace('App\Http\Controllers\Api')->group(function () { 
    // dd('777777');
    Route::get('/governorates', [ApiController::class, 'governorates']);
    Route::get('/regions/{governorate_id}', [ApiController::class, 'regions']);
    Route::get('/districts/{region_id}', [ApiController::class, 'districts']);
    Route::get('/categories', [ApiController::class, 'categories']);
    Route::get('/categories', [ApiController::class, 'categories']);
    Route::get('/filterGroup', [ApiController::class, 'filterGroup']);
    Route::get('/filterGroupCategories/{category_id}', [ApiController::class, 'filterGroupCategories']);
    Route::get('/filters/{filter_group_id}', [ApiController::class, 'filters']);
    Route::get('advertisements/{search?}', [ApiController::class, 'advertisements'])
    ->where('search', '(.*)');

 




// 
    // Route::get('/governorates', ApiController::class);
    // Route::get('/regions', RegionsController::class);
    // Route::resource('/districts', DistrictsController::class);
    // Route::resource('/filters', FiltersController::class);
    // Route::resource('/advertisements', AdvertisementsController::class);
    // Route::resource('/categories', CategoriesController::class);
});