<?php

use App\Models\Region;
use App\Models\Category;
use App\Models\District;
use App\Models\FilterGroup;
use App\Models\Governorate;
use Illuminate\Http\Request;
use App\Models\Advertisement;
use App\Http\Controllers\Admin\FiltersController;
use App\Http\Controllers\Admin\RegionsController;
use App\Http\Controllers\Admin\DistrictsController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\CustomAuthController;
use App\Http\Controllers\Admin\GovernoratesController;
use App\Http\Controllers\Admin\AdvertisementsController;
require 'breadcrumbs.php';



Route::get('login',[CustomAuthController::class,'login'])->name('login');
Route::get('signup',[CustomAuthController::class,'signup'])->name('signup');
Route::post('postlogin',[CustomAuthController::class,'postlogin'])->name('postlogin'); 
Route::post('signupsave', [CustomAuthController::class,'signupsave'])->name('signupsave'); 
Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath','auth' ]
    ], function(){
        Route::resource('governorates', GovernoratesController::class);
        Route::resource('regions', RegionsController::class);
        Route::resource('districts', DistrictsController::class);
        Route::resource('filters', FiltersController::class);
        Route::resource('advertisements', AdvertisementsController::class);
        Route::resource('categories', CategoriesController::class);
        Route::get('getFilters', function (Request $request) {
            $filterGroups = FilterGroup::whereHas('categories', function ($q) use ($request) {
                $q->where('category_id', $request->category_id);
            })->get();
            return view('admin.advertisements.getFilters', compact('filterGroups'));
        })->name('getFilters');

        Route::get('getRegions', function (Request $request) {
            $regions = Region::where('governorate_id', $request->governorate_id)->where('status', 'enabled')->get();
            return view('admin.districts.getRegions', compact('regions'));
        })->name('getRegions');

        Route::get('getDistricts', function (Request $request) {
            $districts = District::where('region_id', $request->region_id)->where('status', 'enabled')->get();
            return view('admin.districts.getDistricts', compact('districts'));
        })->name('getDistricts');
        Route::controller(CustomAuthController::class)->group(function () {
            Route::get('/','home')->name('home'); 
            Route::get('dashboard','dashboard'); 
            Route::get('signout', 'signOut')->name('signout');
            Route::get('forget-password','showForgetPasswordForm')->name('forget.password.get');
            Route::post('forget-password', 'submitForgetPasswordForm')->name('forget.password.post'); 
            Route::get('reset-password/{token}','showResetPasswordForm')->name('reset.password.get');
            Route::post('reset-password','submitResetPasswordForm')->name('reset.password.post');
        });
        
    });
    Route::get('home', function () {
        $categories = Category::all();
        $advertisements = Advertisement::all();
        return view('web.home', compact('categories','advertisements'));
    })->name('home');

    Route::get('getgovernorates', function (Request $request) {
        $datas = Governorate::whereHas('advertisements', function ($q) use ($request) {
            $q->where('category_id', $request->category_id);
        })->get();
        $advertisements = Advertisement::where('category_id', $request->category_id)->get();
        $clickclass = 'governorates';
        return view('web.getdata', compact('datas', 'advertisements', 'clickclass'));
    })->name('getgovernorates');

    Route::get('getregions', function (Request $request) {
        $datas = Region::whereHas('advertisements', function ($q) use ($request) {
            $q->where('category_id', $request->category_id)->where('governorate_id',  $request->governorate_id);
        })->get();
        $advertisements = Advertisement::where('category_id', $request->category_id)->
        where('governorate_id', $request->governorate_id)->get();
        $clickclass = 'regions';
        return view('web.getdata', compact('datas', 'advertisements', 'clickclass'));
    })->name('getregions');

    Route::get('getdistricts', function (Request $request) {
        $datas = District::whereHas('advertisements', function ($q) use ($request) {
            $q->where('category_id', $request->category_id)->where('governorate_id', $request->governorate_id)
            ->where('region_id', $request->region_id);
        })->get();
        $advertisements = Advertisement::where('category_id', $request->category_id)->
        where('governorate_id', $request->governorate_id)->
        where('region_id', $request->region_id)->get();
        $clickclass = 'districts';
        return view('web.getdata', compact('datas', 'advertisements', 'clickclass'));
    })->name('getdistricts');

    Route::get('getdata', function (Request $request) {
        $advertisements = Advertisement::where('category_id', $request->category_id)->
        where('governorate_id', $request->governorate_id)->
        where('region_id', $request->region_id)->
        where('district_id', $request->district_id)->get();
        $filterGroups = FilterGroup::whereHas('categories', function ($q) use ($request) {
            $q->where('category_id', $request->category_id);
        })->get();
        return view('web.advertisements', compact('advertisements','filterGroups'));
    })->name('getdata');

    // Route::get('web','web/index')->name('home');
// Route::controller(WebController::class)->group(function () {

//     // Route::get('dashboard','dashboard'); 
//     // Route::get('signout', 'signOut')->name('signout');
//     // Route::get('forget-password','showForgetPasswordForm')->name('forget.password.get');
//     // Route::post('forget-password', 'submitForgetPasswordForm')->name('forget.password.post'); 
//     // Route::get('reset-password/{token}','showResetPasswordForm')->name('reset.password.get');
//     // Route::post('reset-password','submitResetPasswordForm')->name('reset.password.post');
// });


// Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.', 'middleware' => ['auth', 'checkLogin']], function () {

//     Route::get('/', function () {
//         return view('dashboard.layouts.layout');
//     })->name('index');


//     Route::get('/settings', [SettingController::class, 'index'])->name('settings');

//     Route::post('/settings/update/{setting}', [SettingController::class, 'update'])->name('settings.update');


//     Route::get('/users/all', [UserController::class, 'getUsersDatatable'])->name('users.all');
//     Route::post('/users/delete', [UserController::class, 'delete'])->name('users.delete');


//     Route::get('/category/all', [CategoryController::class, 'getCategoriesDatatable'])->name('category.all');
//     Route::post('/category/delete', [CategoryController::class, 'delete'])->name('category.delete');



//     Route::get('/posts/all', [PostsController::class, 'getPostsDatatable'])->name('posts.all');
//     Route::post('/posts/delete', [PostsController::class, 'delete'])->name('posts.delete');


//     Route::resources([
//         'users' => UserController::class,
//         'category' => CategoryController::class,
//         'posts' => PostsController::class,
//     ]);
// });

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Route::get('/', function () {
//     return view('admin.index');
// });

// Route::get('/app', function () {
//     return view('admin.app');
// });


// Route::get('/gest', function () {
//     return view('admin.gest');
// });

// Route::get('/login', function () {
//     return view('admin.auth.login');
// });


// Route::get('/register', function () {
//     return view('admin.auth.register');
// });


// Route::get('/forgot', function () {
//     return view('admin.auth.forgot');
// });


// Route::get('/reset', function () {
//     return view('admin.auth.reset');
// });


// Route::get('/verify', function () {
//     return view('admin.auth.verify');
// }); 