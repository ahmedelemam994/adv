<?php

namespace App\Http\Controllers\Admin;
use App\Refregion;
use App\Models\Category;
use App\Models\FilterGroup;
use App\Models\Governorate;
use Illuminate\Http\Request;
use App\Models\Advertisement;
use App\Http\Controllers\Controller;


class AdvertisementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the Category
        $advertisements = Advertisement::all();
        return view('admin.advertisements.index', compact('advertisements'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', 'enabled')->get();
        $filterGroups = FilterGroup::where('status', 'enabled')->get();
        $governorates = Governorate::where('status', 'enabled')->get();
        return view('admin.advertisements.create', compact('categories','filterGroups','governorates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = Advertisement::create($request->all()); 
        $data->filters()->attach($this->mapFilters(request('filters')));
        return redirect('advertisements');
    }

    function mapFilters($filters)
    {
        return collect($filters)->map(function ($i) {
            return ['val' => $i ?? 0];
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertisement $advertisement)
    {


    //     $categories = Category::where('status', 'enabled')->get();
    //     $filterGroups = FilterGroup::where('status', 'enabled')->get();
    //     $governorates = Governorate::where('status', 'enabled')->get();

    
    //     return view('admin.categories.edit', compact('Category','categories','filterGroups','item_filtergroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       



        $Category = Category::find($id);
        $Category->update($request->all());
        return redirect('categories');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Category = Category::find($id);
        $Category->delete();

        // redirect
        return redirect('categories');
    }
}
