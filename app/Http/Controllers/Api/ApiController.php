<?php

namespace App\Http\Controllers\Api;
use App\Refregion;
use App\Models\Filter;
use App\Models\Region;
use App\Models\Category;
use App\Models\District;
use App\Models\FilterGroup;
use App\Models\Governorate;
use Illuminate\Http\Request;
use App\Models\Advertisement;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function governorates()
    {
        $governorates = Governorate::where('status', 'enabled')->get();
        return [
            "status" => 1,
            "data" => $governorates
        ];

    }

    public function regions(Request $request)
    {
        $regions = Region::where('status', 'enabled')->where('governorate_id', $request->governorate_id)->get();
        return [
            "status" => 1,
            "data" => $regions
        ];

    }

    public function districts(Request $request)
    {
        $districts = District::where('status', 'enabled')->where('region_id', $request->region_id)->get();
        return [
            "status" => 1,
            "data" => $districts
        ];

    }

    public function categories()
    {
        $categories = Category::where('status', 'enabled')->get();
        return [
            "status" => 1,
            "data" => $categories
        ];

    }

    public function filterGroup()
    {
        $FilterGroup = FilterGroup::where('status', 'enabled')->get();
        return [
            "status" => 1,
            "data" => $FilterGroup
        ];

    }

    
    public function filterGroupCategories(Request $request)
    {

        $filterGroups = FilterGroup::whereHas('categories', function ($q) use ($request) {
            $q->where('category_id', $request->category_id);
        })->get();
        return [
            "status" => 1,
            "data" => $filterGroups
        ];

    }


    public function filters(Request $request)
    {
        $filters = Filter::where('status', 'enabled')->where('filter_group_id', $request->filter_group_id)->get();
        return [
            "status" => 1,
            "data" => $filters
        ];

    }


    // public function advertisements(Request $request)
    // {
    //     foreach($request->all() as $key => $val ){

    //     }
    //     $advertisements = Advertisement::all();
    //     return [
    //         "status" => 1,
    //         "data" => $advertisements
    //     ];

    // }

    public function storeAdvertisement(Request $request)
    {
        $data = Advertisement::create($request->all()); 
        $data->filters()->attach($this->mapFilters(request('filters')));
        return [
            "status" => 1,
            "data" => $data
        ];

    }


    public function advertisements(Request $request)
    {


        $advertisements = Advertisement::whereHas('filters', function($query) use($request) {
            foreach($request->all() as $key => $val ){
                $query->where($key, $val);
            }
        })->get();

        
        return [
            "status" => 1,
            "data" => $advertisements
        ];

    }




}
