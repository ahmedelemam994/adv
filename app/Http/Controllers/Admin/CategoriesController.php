<?php

namespace App\Http\Controllers\Admin;
use App\Refregion;
use App\Models\Category;
use App\Models\FilterGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the Category
        $categories = Category::all();
        return view('admin.categories.index', compact('categories'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::where('status', 'enabled')->get();
        $filterGroups = FilterGroup::where('status', 'enabled')->get();
        return view('admin.categories.create', compact('categories','filterGroups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = Category::create($request->all());        
        $data->filterGroups()->sync(request('Filters'));      
        return redirect('categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $Category)
    {

        $item_filtergroups = $Category->filterGroups->pluck('id')->toArray();
        $categories = Category::where('status', 'enabled')->get();
        $filterGroups = FilterGroup::where('status', 'enabled')->get();
        return view('admin.categories.edit', compact('Category','categories','filterGroups','item_filtergroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $Category)
    {
        $Category->update($request->all());
        $Category->filterGroups()->sync(request('Filters'));      
        return redirect('categories');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $Category)
    {
        $Category->delete();
        return redirect()->back();
    }
}
