<?php

namespace App\Http\Controllers\Admin;
use App\Models\Filter;
use App\Models\FilterGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FiltersController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $filterGroups = FilterGroup::all();
        return view('admin.Filters.index', compact('filterGroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.Filters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $filtergroup = FilterGroup::create([
            'name' => $request->name,
            'type' => $request->type,
            'status' => $request->status
        ]);

        
        foreach ($request->invoice as $invoice) {
            
            Filter::create([
                'name' => $invoice['name'],
                'status' => $invoice['status'],
                'filter_group_id' => $filtergroup->id,
            ]);
        }
        return redirect('filters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $filterGroup = FilterGroup::find($id);
        $filters = Filter::where('filter_group_id', $id)->get();
       
        return view('admin.filters.edit', compact('filterGroup','filters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FilterGroup $FilterGroup, $id)
    {
     

        $FilterGroup->update($request->all());
        foreach ($request->invoice as $invoice) {
            Filter::updateOrCreate([
                'name' => $invoice['name'],
                'status' => $invoice['status'],
                'filter_group_id' => $id,
            ]);
        }
        return redirect('filters');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FilterGroup $FilterGroup)
    {
        $FilterGroup->delete();
        return redirect()->back();
    }
}
