<?php

namespace App\Http\Controllers\Admin;

use App\Models\Region;
use App\Models\District;
use App\Models\Governorate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\DistrictsRequest;



class DistrictsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the Category
        $districts = District::all();
        return view('admin.districts.index', compact('districts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $governorates = Governorate::where('status', 'enabled')->get();
        return view('admin.districts.create', compact('governorates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictsRequest $request)
    {
        District::create($request->validated());
        return redirect('districts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
        $district = District::find($id);
        $region = Region::find($district->region_id);
        $governorates = Governorate::where('status', 'enabled')->get();
        $regions = Region::where('status', 'enabled')->where('governorate_id', $region->governorate_id)->get();
        return view('admin.districts.edit', compact('governorates','regions', 'region', 'district'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(DistrictsRequest $request, District $district) {
        $district->update($request->validated());
        return redirect('regions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        $district->delete();
        return redirect()->back();
    }
}
