<?php

namespace App\Models;

use App\Models\FilterGroup;
use App\Models\Advertisement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Filter extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'filter_group_id', 'status'];

    public function filterGroup()
    {
    	return $this->belongsTo(FilterGroup::class);
    }

    public function advertisements()
    {
        return $this->belongsToMany(Advertisement::class, 'advertisement_filter', 'filter_id','advertisement_id')->withPivot('val');
    }


   
}
