<?php

namespace App\Models;

use App\Models\Region;
use App\Models\Advertisement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Governorate extends Model
{
    use HasFactory;
    protected $fillable = ['name','status'];

    public function regions()
    {
    	return $this->hasMany(Region::class);
    }


    public function advertisements()
    {
    	return $this->hasMany(Advertisement::class);
    }
    
    // public function advertisements()
    // {
    // 	return $this->haseMany(Advertisement::class);
    // }

}

