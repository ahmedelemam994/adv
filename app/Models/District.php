<?php

namespace App\Models;
use App\Models\Region;
use App\Models\Advertisement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class District extends Model
{
    use HasFactory;
    protected $fillable = ['name','status','region_id'];

   

    public function region()
    {
    	return $this->belongsTo(Region::class);
    }

    public function advertisements()
    {
    	return $this->hasMany(Advertisement::class);
    }

}

