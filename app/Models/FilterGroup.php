<?php

namespace App\Models;

use App\Models\Filter;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FilterGroup extends Model
{
    use HasFactory;
    
    protected $fillable = ['name', 'type'];

    public function filters()
    {
    	return $this->hasMany(Filter::class,'filter_group_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_filter_group', 'filter_group_id','category_id');
    }
}
