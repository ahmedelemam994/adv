<?php

namespace App\Models;

use App\Models\FilterGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CategoryFilterGroup extends Model
{
    use HasFactory;
    protected $fillable = ['category_id','filter_group_id'];
}