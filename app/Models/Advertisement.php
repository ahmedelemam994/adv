<?php

namespace App\Models;

// use App\Models\FilterGroup;
use App\Models\Filter;
use App\Models\Region;
use App\Models\Category;
use App\Models\District;
use App\Models\Governorate;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Advertisement extends Model
{
    use HasFactory;
    protected $fillable = ['name','advertiser','price', 'category_id','governorate_id','region_id','district_id'];

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'advertisement_filter', 'advertisement_id','filter_id')->withPivot('val');
    }

    public function governorate()
    {
    	return $this->belongsTo(Governorate::class);
    }


    public function region()
    {
    	return $this->belongsTo(Region::class);
    }
    

    public function district()
    {
    	return $this->belongsTo(District::class);
    }

    
}

