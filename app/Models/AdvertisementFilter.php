<?php

namespace App\Models;

// use App\Models\FilterGroup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AdvertisementFilter extends Model
{
    use HasFactory;
    protected $fillable = ['advertisement_id','filter_id'];
}