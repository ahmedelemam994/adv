<?php

namespace App\Models;

use App\Models\District;
use App\Models\Governorate;
use App\Models\Advertisement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Region extends Model
{
    use HasFactory;
    protected $fillable = ['name','status','governorate_id'];

    public function districts()
    {
    	return $this->hasMany(District::class);
    }

    public function governorate()
    {
    	return $this->belongsTo(Governorate::class);
    }

    public function advertisements()
    {
    	return $this->hasMany(Advertisement::class);
    }

   

}

