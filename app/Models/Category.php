<?php

namespace App\Models;

use App\Models\FilterGroup;
use App\Models\Advertisement;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;
    protected $fillable = ['name','parent_id'];

    public function filterGroups()
    {
        return $this->belongsToMany(FilterGroup::class, 'category_filter_group', 'category_id','filter_group_id');
    }
    
    public function advertisements()
    {
    	return $this->hasMany(Advertisement::class);
    }

}

